package bsk;

import static org.junit.Assert.*;

import org.junit.Test;
import tdd.training.bsk.*;

public class FrameTest {

	@Test
	public void testFrameFirstThrow() throws BowlingException {
		int first = 2;
		int second = 4;
		Frame newFrame = new Frame(first, second);
		assertEquals(2, newFrame.firstThrow);	
	}
	@Test
	public void testFrameSecondThrow() throws BowlingException {
		int first = 2;
		int second = 4;
		Frame newFrame = new Frame(first, second);
		assertEquals(4, newFrame.secondThrow);	
	}
	@Test 
	public void testGetFirstThrow() throws BowlingException {
		int first = 2;
		int second = 4;
		Frame newFrame = new Frame(first, second);
		assertEquals(2, newFrame.getFirstThrow());
	}
	@Test 
	public void testGetSecondThrow() throws BowlingException {
		int first = 2;
		int second = 4;
		Frame newFrame = new Frame(first, second);
		assertEquals(4, newFrame.getSecondThrow());
	}
	@Test 
	public void testGetScore() throws BowlingException {
		int first = 2;
		int second = 4;
		Frame newFrame = new Frame(first, second);
		assertEquals(6, newFrame.getScore());
	}
	@Test
	public void testGetBonus() throws BowlingException {
		Frame newFrame = new Frame(8,2);
		newFrame.setBonus(3);
		assertEquals(3, newFrame.getBonus());
	}
	@Test
	public void testIsStrike() throws BowlingException {
		Frame newFrame = new Frame(10,0);
		assertTrue("This should be true if frame is a strike", newFrame.isStrike());
	}
}
