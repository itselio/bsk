package bsk;

import static org.junit.Assert.*;


import org.junit.Test;
import tdd.training.bsk.*;

public class GameTest {

	@Test
	public void testAddFrame() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,2));
		assertEquals(1, newGame.frames.get(0).firstThrow);
	}
	
	@Test
	public void testGetFirstThrowAtFrame0() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,5));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(1, newGame.getFrameAt(0).getFirstThrow());
	}
	@Test
	public void testGetSecondThrowAtFrame0() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,5));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(5, newGame.getFrameAt(0).getSecondThrow());
	}
	@Test
	public void testCalculateScore() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,5));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(81, newGame.calculateScore());
	}

	@Test 
	public void testIsFrameSpare() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,9));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertTrue("Returns true if Frame is spare", newGame.getFrameAt(0).isSpare());
	}
	@Test 
	public void testIsFrameSpareShouldBeFalse() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(3,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertFalse("Returns false if Frame is not spare", newGame.getFrameAt(0).isSpare());
	}
	@Test public void testGetScoreWithBonus() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,9));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		newGame.calculateScore();
		assertEquals(13, newGame.getFrameAt(0).getScore());
	}
	
	@Test 
	public void testCalculateScoreWithBonus() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,9));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(88, newGame.calculateScore());
	}
	@Test
	public void testGetScoreWithStrike() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		newGame.calculateScore();
		assertEquals(19, newGame.getFrameAt(0).getScore());
	}
	@Test
	public void testCalculateScoreWithStrike() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(94, newGame.calculateScore());
	}
	@Test
	public void testCalculateScoreWithStrikeAndSpare() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(4,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(103, newGame.calculateScore());
	}
	@Test
	public void testGetScoreWithDoubleStrike() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		newGame.calculateScore();
		assertEquals(27, newGame.getFrameAt(0).getScore());
	}
	@Test
	public void testCalculateScoreWithDoubleStrike() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		assertEquals(112, newGame.calculateScore());
	}
	@Test
	public void testGetScoreWithDoubleSpare() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(8,2));
		newGame.addFrame(new Frame(5,5));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		newGame.calculateScore();
		assertEquals(15, newGame.getFrameAt(0).getScore());
	}
	@Test
	public void testGetScoreWithDoubleSpare2() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(8,2));
		newGame.addFrame(new Frame(5,5));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,6));
		newGame.calculateScore();
		assertEquals(17, newGame.getFrameAt(1).getScore());
	}
	
	@Test
	public void testSetGetFirstBonusThrow() throws BowlingException {
		Game newGame = new Game();
		newGame.setFirstBonusThrow(5);
		assertEquals(5, newGame.getFirstBonusThrow());
	}
	@Test
	public void testCalculateScoreWithSpareAtLastFrame() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,5));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(2,8));
		newGame.setFirstBonusThrow(7);
		assertEquals(90, newGame.calculateScore());
	}
	
	@Test
	public void testSetGetSecondBonusThrow() throws BowlingException {
		Game newGame = new Game();
		newGame.setSecondBonusThrow(7);
		assertEquals(7, newGame.getSecondBonusThrow());
	}
	
	@Test
	public void testCalculateScoreWithStrikeAtLastFrame() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(1,5));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(7,2));
		newGame.addFrame(new Frame(3,6));
		newGame.addFrame(new Frame(4,4));
		newGame.addFrame(new Frame(5,3));
		newGame.addFrame(new Frame(3,3));
		newGame.addFrame(new Frame(4,5));
		newGame.addFrame(new Frame(8,1));
		newGame.addFrame(new Frame(10,0));
		newGame.setFirstBonusThrow(7);
		newGame.setSecondBonusThrow(2);
		assertEquals(92, newGame.calculateScore());
	}
	@Test
	public void testCalculateBestScore() throws BowlingException {
		Game newGame = new Game();
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.addFrame(new Frame(10,0));
		newGame.setFirstBonusThrow(10);
		newGame.setSecondBonusThrow(10);
		assertEquals(300, newGame.calculateScore());
	}
	
}
