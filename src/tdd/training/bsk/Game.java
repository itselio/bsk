package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {

	public List<Frame> frames = new ArrayList<>();
	private int score = 0;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		// To be implemented
		return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		// To be implemented
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		if(checkBestScore()) {
			return 300;
		}
		
		if(frames.get(9).isSpare()) {
			score += getFirstBonusThrow();
		}
		if(frames.get(9).isStrike()) {
			score += getFirstBonusThrow() + getSecondBonusThrow();
		}
		for (int i = 8; i >= 0; i--) {
			if(frames.get(i).isSpare()) {
				frames.get(i).setBonus(frames.get(i+1).getFirstThrow());
			} else if(frames.get(i).isStrike()) {
				if(frames.get(i+1).isStrike()) {
					frames.get(i).setBonus((frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow()) + frames.get(i+2).getFirstThrow());
				} else {					
					frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow());
				}
			}
		}
		for (int c = 0; c <= 9; c++) {
			score += frames.get(c).getScore();
		}
		return score;
	}
	private boolean checkBestScore() {
		for(int i = 0; i <= 9; i++) {
			if(!(frames.get(i).isStrike())) {				
				return false;
			}
		}
		if(firstBonusThrow == 10 && secondBonusThrow == 10) {			
			return true;
		}
		return false;
	}
}
